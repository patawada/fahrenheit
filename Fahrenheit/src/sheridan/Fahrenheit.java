package sheridan;

public class Fahrenheit {

	public static void main(String[] args) {

		System.out.println("11 degrees in Celsius is " + convertFromCelsius(11));

	}

	public static int convertFromCelsius(int tempC) {

		int tempF = (int) Math.ceil((tempC * 1.8) + 32);
		return tempF;
	}

}
