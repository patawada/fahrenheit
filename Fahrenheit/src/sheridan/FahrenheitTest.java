package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromCelsius() {
		Fahrenheit fahrenheit = new Fahrenheit();
		int tempF = fahrenheit.convertFromCelsius(10);
		assertTrue("This is correct.", tempF == 50);
	}

	@Test
	public void testNotConvertFromCelsius() {
		Fahrenheit fahrenheit = new Fahrenheit();
		int tempF = fahrenheit.convertFromCelsius(-3);
		assertFalse("This is correct.", tempF == 50);
	}

	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		Fahrenheit fahrenheit = new Fahrenheit();
		int tempF = fahrenheit.convertFromCelsius(0);
		assertTrue("Invalid input.", tempF == 32);
	}

	@Test
	public void testConvertFromCelsiusBoundaryOut() {
		Fahrenheit fahrenheit = new Fahrenheit();
		int tempF = fahrenheit.convertFromCelsius(0);
		assertFalse("Invalid input.", tempF == 60);
	}

}
